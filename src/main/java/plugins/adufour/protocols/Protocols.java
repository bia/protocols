package plugins.adufour.protocols;

import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import icy.common.Version;
import icy.file.FileUtil;
import icy.main.Icy;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.PluginActionable;
import icy.preferences.PluginsPreferences;
import icy.preferences.XMLPreferences;
import icy.system.thread.ThreadUtil;
import icy.util.XMLUtil;
import plugins.adufour.blocks.lang.WorkFlow;
import plugins.adufour.blocks.util.BlocksException;
import plugins.adufour.blocks.util.BlocksML;
import plugins.adufour.blocks.util.BlocksReloadedException;
import plugins.adufour.protocols.gui.MainFrame;
import plugins.adufour.protocols.gui.ProtocolPanel;

/**
 * Protocols plugin
 */
public class Protocols extends PluginActionable
{
    // "command" on mac, "ctrl" on others
    public static final int MENU_SHORTCUT_KEY;

    static
    {
        MENU_SHORTCUT_KEY = GraphicsEnvironment.isHeadless() ? 0 : Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
    }

    private static final String PREF_FOLDER = "protocolFolder";

    private static MainFrame mainFrame;

    private static boolean reloading = false;

    public static final String downloadedProtocolFolder = FileUtil.getApplicationDirectory() + File.separator
            + "protocols";

    private static volatile XMLPreferences preferences = PluginsPreferences.getPreferences()
            .node(Protocols.class.getName());

    public static String getDefaultProtocolFolder()
    {
        return preferences.get(PREF_FOLDER, System.getProperty("user.home"));
    }

    public static void setDefaultProtocolFolder(final String path)
    {
        preferences.put(PREF_FOLDER, path);
    }

    // public static MainFrame getLastActiveFrame()
    // {
    // return openedFrames.peek();
    // }

    /**
     * @return <i>Protocols</i> main instance and <code>null</code> if Protocols is not started
     */
    public static MainFrame getInstance()
    {
        return mainFrame;
    }

    public static boolean isReloading()
    {
        return reloading;
    }

    public static void setReloading(final boolean value)
    {
        reloading = value;
    }

    /**
     * Saves the current state of the Protocols interface and restarts it.
     * <p>
     * This method is useful when plug-ins have been modified (via the plug-in
     * loader) and requires Protocols to restart to take into account the new
     * changes
     * 
     * @throws TransformerFactoryConfigurationError
     */
    public void reload(final Document reloadingXML, final String reloadingPath)
    {
        // 0) avoid silly situations...
        if (mainFrame == null)
            return;

        reloading = true;

        // 1) save the current state with all opened protocols into the preferences

        preferences.putInt("Window X", mainFrame.getX());
        preferences.putInt("Window Y", mainFrame.getY());

        int counter = 1;

        // save protocols to the preferences file one by one
        for (final ProtocolPanel protocol : mainFrame.getProtocolPanels())
        {
            try
            {
                final File attachedFile = protocol.getFile();
                String xmlProtocol;

                if (attachedFile != null && attachedFile.getAbsolutePath().equals(reloadingPath))
                {
                    xmlProtocol = BlocksML.getInstance().toString(reloadingXML);
                }
                else
                {
                    xmlProtocol = BlocksML.getInstance().toString(protocol.getWorkFlow());
                }

                final XMLPreferences node = preferences.node("Protocol #" + counter++);
                node.putBoolean("dirty", protocol.isDirty());
                node.put("xml", xmlProtocol);
                if (attachedFile != null)
                    node.put("fileName", attachedFile.getAbsolutePath());
            }
            catch (final TransformerException e)
            {
                System.err.println("Warning: couldn't store protocol " + protocol.getName() + ":");
                e.printStackTrace();
                System.err.println("---");
            }
        }

        // 2) close the current Protocols instance

        close();

        ThreadUtil.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                // 3) launch a new instance of the Protocols plug-in
                try
                {
                    ((PluginActionable) PluginLoader.getPluginClass(Protocols.class.getName()).newInstance()).run();
                }
                catch (final Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private final static LinkedHashMap<String, String> commandLineArguments = new LinkedHashMap<String, String>();

    public static Map<String, String> getCommandLineArguments()
    {
        return commandLineArguments;
    }

    @Override
    public void run()
    {
        if (Icy.getMainInterface().isHeadLess())
        {
            // Command line perhaps?
            runHeadless();
            return;
        }

        if (mainFrame != null)
        {
            mainFrame.requestFocus();
            mainFrame.toFront();
            return;
        }

        mainFrame = new MainFrame(this);

        try
        {
            final int x = preferences.getInt("Window X", mainFrame.getX());
            final int y = preferences.getInt("Window Y", mainFrame.getY());
            mainFrame.setLocation(x, y);

            final String protocolPath = parseCommandLineArgs(false);

            // we have a protocol filename specified ?
            if (protocolPath != null)
            {
                final ProtocolPanel panel = new ProtocolPanel(mainFrame);
                final Document xml = XMLUtil.loadDocument(protocolPath);

                // Discard invalid files
                if (xml != null)
                    panel.setFile(new File(protocolPath));

                mainFrame.addProtocolPane(panel);

                if (xml != null)
                {
                    try
                    {
                        panel.loadWorkFlow(xml, true);
                    }
                    catch (final BlocksReloadedException e)
                    {
                        reload(xml, panel.getFile().getAbsolutePath());
                        return;
                    }
                    catch (BlocksException e2)
                    {
                        e2.printStackTrace();
                    }
                }
            }
            else
            {
                // Reload potential temporary protocols from the preferences
                final ArrayList<XMLPreferences> protocols = preferences.getChildren();

                // no protocol info in XML preference
                if (protocols.size() == 0)
                    mainFrame.addProtocolPane(new ProtocolPanel(mainFrame));
                else
                {
                    final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                    for (final XMLPreferences node : preferences.getChildren())
                    {
                        final ProtocolPanel panel = new ProtocolPanel(mainFrame);
                        final String fileName = node.get("fileName", null);
                        if (fileName != null)
                            panel.setFile(new File(fileName));
                        mainFrame.addProtocolPane(panel);

                        final Document xml = builder.parse(new InputSource(new StringReader(node.get("xml", null))));

                        try
                        {
                            panel.loadWorkFlow(xml, node.getBoolean("dirty", false));

                            // if the protocol loads correctly, remove it from the preferences
                            preferences.remove(node.name());
                        }
                        catch (final BlocksReloadedException e)
                        {
                            reload(xml, panel.getFile().getAbsolutePath());
                            return;
                        }
                        catch (BlocksException e2)
                        {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        }
        catch (final ParserConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (final SAXException e)
        {
            e.printStackTrace();
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }

        mainFrame.addToDesktopPane();
        mainFrame.setVisible(true);
    }

    /**
     * Build the command line arguments map and return protocol path if any is specified
     */
    private static String parseCommandLineArgs(final boolean verify)
    {
        final String[] clargs = Icy.getCommandLinePluginArgs();
        String result = null;
        boolean ok = false;

        for (final String clarg : clargs)
        {
            // Sanity check
            if (!clarg.contains("="))
            {
                if (verify)
                    throw new IllegalArgumentException("Invalid command line argument: " + clarg);

                // next arg
                continue;
            }

            final String[] keyValuePair = clarg.split("=");
            if (keyValuePair.length != 2)
            {
                if (verify)
                    throw new IllegalArgumentException("Invalid command line argument: " + clarg);

                // next arg
                continue;
            }

            final String key = keyValuePair[0];
            final String value = keyValuePair[1];

            if (key.isEmpty())
            {
                if (verify)
                    throw new IllegalArgumentException("Invalid command line argument (no key): " + clarg);

                // next arg
                continue;
            }

            if (value.isEmpty())
            {
                if (verify)
                    throw new IllegalArgumentException("Invalid command line argument (no value): " + clarg);

                // next arg
                continue;
            }

            if (key.equalsIgnoreCase("protocol"))
            {
                result = value;
                ok = true;
            }
            else
                commandLineArguments.put(key, value);
        }

        // we correctly parsed arguments ? --> clear it so no other plugin can re-parse them
        if (ok)
            Icy.clearCommandLinePluginArgs();

        return result;
    }

    private static void runHeadless()
    {
        final String protocolFile = parseCommandLineArgs(true);

        final Document xml = XMLUtil.loadDocument(protocolFile);

        // Discard invalid files
        if (xml == null)
            throw new IllegalArgumentException(protocolFile + " is not a valid protocol file");

        System.out.println("Loading workflow...");
        final WorkFlow workFlow = new WorkFlow(true);
        BlocksML.getInstance().loadWorkFlow(xml, workFlow);

        workFlow.run();
    }

    public static void loadWorkFlow(final File file)
    {
        if (mainFrame == null)
            new Protocols().run();

        mainFrame.loadWorkFlow(file);
    }

    public static void close()
    {
        if (mainFrame != null && mainFrame.isVisible())
            mainFrame.close();
        mainFrame = null;
    }

    public static void dispatchEvent(final KeyEvent key)
    {
        if (mainFrame != null)
            mainFrame.getContentPane().dispatchEvent(key);
    }

    public String getFriendlyVersion()
    {
        Version v = getDescriptor().getVersion();
        return "Blocks engine v." + v.getMajor() + "." + v.getMinor();
    }

}
