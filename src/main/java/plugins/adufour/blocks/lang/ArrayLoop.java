package plugins.adufour.blocks.lang;

/**
 * @deprecated use {@link Batch} instead.
 * @author Alexandre Dufour
 */
@Deprecated
public class ArrayLoop extends Batch
{}