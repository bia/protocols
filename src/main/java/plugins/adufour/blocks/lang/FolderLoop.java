package plugins.adufour.blocks.lang;

/**
 * @deprecated use {@link FileBatch} instead.
 * @author Alexandre Dufour
 */
@Deprecated
public class FolderLoop extends FileBatch
{}
