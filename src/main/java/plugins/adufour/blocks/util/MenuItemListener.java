package plugins.adufour.blocks.util;

import icy.plugin.PluginDescriptor;

public interface MenuItemListener {
	public void displayDoc(PluginDescriptor d);
}
