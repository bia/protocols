package plugins.adufour.blocks.tools.io;

import plugins.adufour.blocks.lang.Block;

/**
 * Interface used to mark blocks as I/O blocks (and appear in the appropriate menu)
 * 
 * @author Alexandre Dufour
 * 
 */
public interface IOBlock extends Block
{
    
}
