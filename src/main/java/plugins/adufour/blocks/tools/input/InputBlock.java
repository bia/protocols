package plugins.adufour.blocks.tools.input;

import plugins.adufour.blocks.lang.Block;

/**
 * Interface used to flag pure input blocks
 * 
 * @author Alexandre Dufour
 * 
 */
public interface InputBlock extends Block
{
}