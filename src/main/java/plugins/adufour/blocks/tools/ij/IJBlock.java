package plugins.adufour.blocks.tools.ij;

import plugins.adufour.blocks.lang.Block;

/**
 * Interface used to flag ImageJ-related blocks
 * 
 * @author Alexandre Dufour
 * 
 */
public interface IJBlock extends Block
{
}