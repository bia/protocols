package plugins.adufour.blocks.tools.output;

import plugins.adufour.blocks.lang.Block;

/**
 * Interface used to flag pure output blocks
 * 
 * @author Stephane Dallongeville
 * 
 */
public interface OutputBlock extends Block
{
}