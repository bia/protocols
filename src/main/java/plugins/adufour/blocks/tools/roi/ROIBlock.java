package plugins.adufour.blocks.tools.roi;

import plugins.adufour.blocks.lang.Block;

/**
 * Interface used to flag ROI-related blocks
 * 
 * @author Alexandre Dufour
 * 
 */
public interface ROIBlock extends Block
{
    
}
