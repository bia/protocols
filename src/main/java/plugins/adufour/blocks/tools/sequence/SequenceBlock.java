package plugins.adufour.blocks.tools.sequence;

import plugins.adufour.blocks.lang.Block;

/**
 * Interface used to flag Sequence-related blocks
 * 
 * @author Alexandre Dufour
 * 
 */
public interface SequenceBlock extends Block
{
    
}
