# Protocols

<!-- badges: start -->
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Twitter](https://img.shields.io/twitter/follow/Icy_BioImaging?style=social)](https://twitter.com/Icy_BioImaging)
[![Image.sc forum](https://img.shields.io/badge/discourse-forum-brightgreen.svg?style=flat)](https://forum.image.sc/tag/icy)
<!-- badges: end -->

This is the repository for the source code of *Protocols*, a plugin for the [bioimage analysis software Icy](http://icy.bioimageanalysis.org/), which was developed by members or former members of the [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). This plugin is licensed under GPL3 license.     
Icy is developed and maintained by [Biological Image Analysis unit at Institut Pasteur](https://research.pasteur.fr/en/team/bioimage-analysis/). The [source code of Icy](https://gitlab.pasteur.fr/bia/icy) is also licensed under a GPL3 license.     



## Plugin description

<!-- Short description of goals of package, with descriptive links to the documentation website --> 

Design your own <strong>bioimage analysis workflow, graphically, </strong>and automate it on many images (<strong>batch processing</strong>).<strong>
</strong>

No programming knowledge required! Protocols can be shared and re-used straight from the Icy website.

Watch this 30 minutes<a href="https://www.youtube.com/watch?v=5SJqhKjCbMQ"> introduction to protocols</a> during the NEUBIAS Academy @home webinar (go directly to <a class="yt-simple-endpoint style-scope yt-formatted-string" dir="auto" spellcheck="false" href="https://www.youtube.com/watch?v=5SJqhKjCbMQ&amp;t=1758s">29:18</a> to skip the general introduction to Icy) and see the corresponding slides (<a href="https://marionlouveaux.github.io/bioimage_analysis_with_icy/">whole webinar</a> or <a href="https://marionlouveaux.github.io/bioimage_analysis_with_icy/#23">protocol part only</a>).        
A more detailed user documentation can be found on the Protocols documentation page on the Icy website: http://icy.bioimageanalysis.org/plugin/protocols/               


## Installation instructions

For end-users, refer to the documentation on the Icy website on [how to install an Icy plugin](http://icy.bioimageanalysis.org/tutorial/how-to-install-an-icy-plugin/).      

For developers, see our [Contributing guidelines](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CONTRIBUTING.md) and [Code of Conduct](https://gitlab.pasteur.fr/bia/icy/-/blob/master/CODE-OF-CONDUCT.md).      

<!--  Here we should have some explanations on how to fork this repo (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Add any info related to Maven etc. How the project is build (for an example see https://gitlab.pasteur.fr/bia/wellPlateReader). Any additional setup required (authentication tokens, etc).  -->


## Main functions and usage

<!-- list main functions, explain architecture, classname, give info on how to get started with the plugin. If applicable, how the package compares to other similar packages and/or how it relates to other packages -->

Classname: `plugins.adufour.protocols.Protocols`



## Citation 

Please cite this plugins as follows:          


Please also cite the Icy software and mention the version of Icy you used (bottom right corner of the GUI or first lines of the Output tab):     
de Chaumont, F. et al. (2012) Icy: an open bioimage informatics platform for extended reproducible research, [Nature Methods](https://www.nature.com/articles/nmeth.2075), 9, pp. 690-696       
http://icy.bioimageanalysis.org    



## Author(s)      

Alexandre Dufour, Stephane Dallongeville


## Additional information






